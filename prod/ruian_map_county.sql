SET NAMES utf8mb4;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET FOREIGN_KEY_CHECKS = 0;
SET AUTOCOMMIT = 0;

START TRANSACTION;
SET time_zone = "+00:00";

BEGIN;
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3017, 'Kraj Vysočina', 'kraj-vysocina');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3018, 'Hlavní město Praha', 'hlavni-mesto-praha');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3026, 'Středočeský kraj', 'stredocesky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3034, 'Jihočeský kraj', 'jihocesky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3042, 'Plzeňský kraj', 'plzensky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3051, 'Karlovarský kraj', 'karlovarsky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3069, 'Ústecký kraj', 'ustecky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3077, 'Liberecký kraj', 'liberecky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3085, 'Královéhradecký kraj', 'kralovehradecky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3093, 'Pardubický kraj', 'pardubicky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3115, 'Jihomoravský kraj', 'jihomoravsky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3123, 'Olomoucký kraj', 'olomoucky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3131, 'Zlínský kraj', 'zlinsky-kraj');
INSERT INTO  `ruian_map_county` (`county_code`, `county_name`, `slug`) VALUES (3140, 'Moravskoslezský kraj', 'moravskoslezsky-kraj');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
