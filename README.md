# MySQL RÚIAN data
![CC-BY-4.0](https://img.shields.io/badge/licence-CC--BY--4.0-brightgreen)

This package provides RÚIAN data in MySQL format, useful for example with arodax/ruian-bundle.

## Changelog

### 1.2.0
- requires ruian-bundle ^3.2
- ruian_map_district.sql added dictrict capiitals, execute this SQL file to update district data

## Licence
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>. 